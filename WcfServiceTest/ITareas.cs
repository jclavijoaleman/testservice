﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceTest
{
    [ServiceContract]
    public interface ITareas
    {
        [OperationContract]
        bool AddTareas(Tarea tarea);

        [OperationContract]
        DataSet GetTareas(string idCliente);

        [OperationContract]
        bool DeleteTareas(Tarea tarea);
        
        [OperationContract]
        bool UpdateTareas(Tarea tarea);

    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Tarea
    {

        int _id;
        int _idMascota;
        int _idTarea;
        DateTime _fecha;
        DateTime _hora;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public int IdMascota
        {
            get { return _idMascota; }
            set { _idMascota = value; }
        }

        [DataMember]
        public int IdTarea
        {
            get { return _idTarea; }
            set { _idTarea = value; }
        }

        [DataMember]
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        [DataMember]
        public DateTime Hora
        {
            get { return _hora; }
            set { _hora = value; }
        }
       
    }
}
