﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceTest
{
    [ServiceContract]
    public interface IMascotas
    {
        [OperationContract]
        Mascota AddMascota(Mascota mascota);

        [OperationContract]
        DataSet GetMascotas(string idCliente, string idTienda);

        [OperationContract]
        bool DeleteMascota(Mascota mascota);

        [OperationContract]
        DataSet SearchMascota(Mascota mascota);

        [OperationContract]
        Mascota UpdateMascota(Mascota mascota);

        [OperationContract]
        Mascota ValidarMascota(Mascota mascota);

        [OperationContract]
        DataSet GetTareasMascotas(string nombreMascota, string nroTareas);
    }
    
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Mascota
    {

        int _id;
        string _apodo = "";
        string _nombre = "";
        DateTime _fecha;
        byte[] _foto;
        string _sexo = "";
        string _raza;
        int _idCliente;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string Apodo
        {
            get { return _apodo; }
            set { _apodo = value; }
        }

        [DataMember]
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        
        [DataMember]
        public DateTime FechaNacimiento
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        [DataMember]
        public byte[] Foto
        {
            get { return _foto; }
            set { _foto = value; }
        }

        [DataMember]
        public string Sexo
        {
            get { return _sexo; }
            set { _sexo = value; }
        }

        [DataMember]
        public string Raza
        {
            get { return _raza; }
            set { _raza = value; }
        }

        [DataMember]
        public int IdCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }
    }
}
