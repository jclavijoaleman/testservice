﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IUsuarios
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        Usuario AddUsuario(Usuario emp, bool registro = false);

        [OperationContract]
        DataSet GetUsuarios();

        [OperationContract]
        bool DeleteUsuario(Usuario emp);

        [OperationContract]
        DataSet SearchUsuario(Usuario emp);

        [OperationContract]
        Usuario UpdateUsuario(Usuario emp);

        [OperationContract]
        Usuario ValidarUsuario(Usuario emp, bool registro = false);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Usuario
    {

        int _id;
        long _cedula;
        string _nombre = "";
        string _apellido = "";
        string _direccion = "";
        DateTime _fecha;
        string _email = "";
        string _sexo = "";
        long _telefono;
        string _password = "";
        byte[] _foto;
        int _idTienda;
        int _idPerfil;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public long Cedula
        {
            get { return _cedula; }
            set { _cedula = value; }
        }

        [DataMember]
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        [DataMember]
        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }

        [DataMember]
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        [DataMember]
        public DateTime FechaNacimiento
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember]
        public string Sexo
        {
            get { return _sexo; }
            set { _sexo = value; }
        }

        [DataMember]
        public long Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        [DataMember]
        public byte[] Foto
        {
            get { return _foto; }
            set { _foto = value; }
        }

        [DataMember]
        public int IdTienda
        {
            get { return _idTienda; }
            set { _idTienda = value; }
        }

        [DataMember]
        public int IdPerfil
        {
            get { return _idPerfil; }
            set { _idPerfil = value; }
        }
    }
}
