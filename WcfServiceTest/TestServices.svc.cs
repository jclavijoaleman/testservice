﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Win32;

namespace WcfServiceTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class TestServices : IUsuarios, IMascotas, ITareas
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        #region Clientes
        public Usuario AddUsuario(Usuario usu, bool registro = false)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                if (registro)
                {
                    string Query = @"INSERT INTO Usuarios ([Cedula], [Password], [IdPerfil])  
                                                  Values( @Cedula,  @Password, @IdPerfil)";

                    cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@Cedula", usu.Cedula);
                    cmd.Parameters.AddWithValue("@Password", usu.Password);
                    cmd.Parameters.AddWithValue("@IdPerfil", usu.IdPerfil);
                }
                else
                {
                    string Query = @"INSERT INTO Usuarios (Cedula, Nombre, Apellido, Direccion, FechaNacimiento, Email, Sexo, Telefono, Password, " + ( usu.Foto == null ? "" : " Foto, ") + " IdTienda, IdPerfil) " +
                                                  " Values( @Cedula, @Nombre, @Apellido, @Direccion, @FechaNacimiento, @Email, @Sexo, @Telefono, @Password," + (usu.Foto == null ? "" : " @Foto, ") + " @IdTienda, @IdPerfil)";

                    cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@Cedula", usu.Cedula);
                    cmd.Parameters.AddWithValue("@Nombre", usu.Nombre);
                    cmd.Parameters.AddWithValue("@Apellido", usu.Apellido);
                    cmd.Parameters.AddWithValue("@Direccion", usu.Direccion);
                    cmd.Parameters.AddWithValue("@FechaNacimiento", usu.FechaNacimiento);
                    cmd.Parameters.AddWithValue("@Email", usu.Email);
                    cmd.Parameters.AddWithValue("@Sexo", usu.Sexo);
                    cmd.Parameters.AddWithValue("@Telefono", usu.Telefono);
                    cmd.Parameters.AddWithValue("@Password", usu.Password);
                    if (usu.Foto != null)
                        cmd.Parameters.AddWithValue("@Foto", usu.Foto);
                    cmd.Parameters.AddWithValue("@IdTienda", usu.IdTienda);
                    cmd.Parameters.AddWithValue("@IdPerfil", usu.IdPerfil);
                }
                

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                Usuario user = ValidarUsuario(usu, registro);
                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public DataSet GetUsuarios()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT *, CONVERT(varchar(10), u.FechaNacimiento, 103) FechaNac FROM Usuarios u inner join tiendas t on t.IdTienda = u.IdTienda inner join Ciudades c on c.IdCiudad = t.IdCiudad";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool DeleteUsuario(Usuario usu)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = "DELETE FROM Usuarios Where Id = @Id";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@Id", usu.Id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        public DataSet SearchUsuario(Usuario usu)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT *, CONVERT(varchar(10), u.FechaNacimiento, 103) FechaNac FROM Usuarios u left join tiendas t on t.IdTienda = u.IdTienda left join Ciudades c on c.IdCiudad = t.IdCiudad where id = @Id";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.SelectCommand.Parameters.AddWithValue("@Id", usu.Id);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public Usuario ValidarUsuario(Usuario usu, bool registro = false)
        {
            DataSet ds = new DataSet();
            Usuario user;

            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT * FROM Usuarios WHERE cedula = @Cedula ";
                if (!string.IsNullOrEmpty(usu.Password))
                    Query += " and password = @Password ";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.SelectCommand.Parameters.AddWithValue("@Cedula", usu.Cedula);
                if (!string.IsNullOrEmpty(usu.Password))
                    sda.SelectCommand.Parameters.AddWithValue("@Password", usu.Password);

                sda.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                if (registro)
                {
                    user = (from DataRow dr in ds.Tables[0].Rows
                        select new Usuario()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Cedula = Convert.ToInt64(dr["Cedula"]),
                            Password = dr["Password"].ToString(),
                            IdPerfil = Convert.ToInt32(dr["IdPerfil"])
                        }).FirstOrDefault();

                    return user;
                }
                
                user = (from DataRow dr in ds.Tables[0].Rows
                                select new Usuario()
                                {
                                    Id = Convert.ToInt32(dr["Id"]),
                                    Cedula = Convert.ToInt64(dr["Cedula"]),
                                    Nombre = dr["Nombre"]?.ToString() ?? "",
                                    Apellido = dr["Apellido"]?.ToString() ?? "",
                                    Direccion = dr["Direccion"]?.ToString() ?? "",
                                    FechaNacimiento = Convert.ToDateTime(dr["FechaNacimiento"] ?? DateTime.Now),
                                    Email = dr["Email"]?.ToString() ?? "",
                                    Sexo = dr["Sexo"]?.ToString() ?? "",
                                    Telefono = Convert.ToInt64(dr["Telefono"] ?? 0),
                                    IdTienda = Convert.ToInt32(dr["IdTienda"] ?? 0),
                                    IdPerfil = Convert.ToInt32(dr["IdPerfil"])
                                }).FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public Usuario UpdateUsuario(Usuario usu)
        {
            try
            {

                SqlConnection con =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string query = "UPDATE Usuarios SET [Cedula] = @Cedula "
                               + ",[Nombre] = @Nombre "
                               + ",[Apellido] = @Apellido "
                               + ",[Direccion] = @Direccion "
                               + ",[FechaNacimiento] = @FechaNacimiento "
                               + ",[Email] = @Email "
                               + ",[Sexo] = @Sexo "
                               + ",[Telefono] = @Telefono "
                               + ",[IdTienda] = @IdTienda "
                               + ",[IdPerfil] = @IdPerfil ";

                if (!string.IsNullOrEmpty(usu.Password))
                    query += ",[Password] = @Password ";

                if (usu.Foto != null)
                    query += ",[Foto] = @Foto ";

                query += " WHERE Id = @Id";

                cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@Id", usu.Id);
                cmd.Parameters.AddWithValue("@Cedula", usu.Cedula);
                cmd.Parameters.AddWithValue("@Nombre", usu.Nombre);
                cmd.Parameters.AddWithValue("@Apellido", usu.Apellido);
                cmd.Parameters.AddWithValue("@Direccion", usu.Direccion);
                cmd.Parameters.AddWithValue("@FechaNacimiento", usu.FechaNacimiento);
                cmd.Parameters.AddWithValue("@Email", usu.Email);
                cmd.Parameters.AddWithValue("@Sexo", usu.Sexo);
                cmd.Parameters.AddWithValue("@Telefono", usu.Telefono);
                cmd.Parameters.AddWithValue("@IdTienda", usu.IdTienda);
                cmd.Parameters.AddWithValue("@IdPerfil", usu.IdPerfil);

                if (!string.IsNullOrEmpty(usu.Password))
                    cmd.Parameters.AddWithValue("@Password", usu.Password);
                if (usu.Foto != null)
                    cmd.Parameters.AddWithValue("@Foto", usu.Foto);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                Usuario user = ValidarUsuario(usu);
                return user;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Mascotas

        public Mascota AddMascota(Mascota usu)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();
                
                string Query = @"INSERT INTO Mascotas (Apodo, NombreMascota, FechaNacimiento," + (usu.Foto == null ? "" : " Foto, ") + " Sexo, Raza, IdCliente) " +
                                            " Values( @Apodo, @NombreMascota, @FechaNacimiento, " + (usu.Foto == null ? "" : " @Foto, ") + " @Sexo, @Raza, @IdCliente)";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@Apodo", usu.Apodo);
                cmd.Parameters.AddWithValue("@NombreMascota", usu.Nombre);
                cmd.Parameters.AddWithValue("@FechaNacimiento", usu.FechaNacimiento);
                
                if (usu.Foto != null)
                    cmd.Parameters.AddWithValue("@Foto", usu.Foto);

                cmd.Parameters.AddWithValue("@Sexo", usu.Sexo);
                cmd.Parameters.AddWithValue("@Raza", usu.Raza);
                cmd.Parameters.AddWithValue("@IdCliente", usu.IdCliente);
                
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                Mascota mas = ValidarMascota(usu);
                return mas;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public DataSet GetMascotas(string idCliente, string idTienda)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT m.*, u.id, u.Cedula, u.Nombre, u.Apellido, u.IdTienda, CONVERT(varchar(10), m.FechaNacimiento, 103) FechaNac, " +
                                " (select count(*) from Tareasmascotas tm where estado = 0 and tm.idMascota = m.idMascota) pendientes, " +
                                " (select count(*) from Tareasmascotas tm where tm.idMascota = m.idMascota) asignadas " +
                                " FROM Mascotas m " +
                                " inner join Usuarios u on m.IdCliente = u.Id " +
                                " left join tiendas t on t.idtienda = u.idtienda  " +
                                " where 1 = 1 ";

                if (!string.IsNullOrEmpty(idCliente)) Query += " And idCliente = " + idCliente;
                if (!string.IsNullOrEmpty(idTienda)) Query += " And idTienda = " + idTienda;

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public DataSet GetTareasMascotas(string nombre, string num)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT m.*, u.id, u.Cedula, u.Nombre, u.Apellido, u.IdTienda, CONVERT(varchar(10), m.FechaNacimiento, 103) FechaNac, " +
                               " (select count(*) from Tareasmascotas tm where estado = 0 and tm.idMascota = m.idMascota) pendientes, " +
                               " (select count(*) from Tareasmascotas tm where tm.idMascota = m.idMascota) asignadas " +
                               " FROM Mascotas m " +
                               " inner join Usuarios u on m.IdCliente = u.Id " +
                               " left join tiendas t on t.idtienda = u.idtienda  " +
                               " where 1 = 1 ";

                if (!string.IsNullOrEmpty(nombre)) Query += " And idCliente = " + nombre;
                if (!string.IsNullOrEmpty(num)) Query += " And idTienda = " + num;

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool DeleteMascota(Mascota usu)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = "DELETE FROM Mascotas Where idMascota = @idMascota";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@idMascota", usu.Id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataSet SearchMascota(Mascota usu)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT *, CONVERT(varchar(10), m.FechaNacimiento, 103) FechaNac FROM Mascotas m inner join Usuarios u on m.IdCliente = u.Id where idMascota = @Id";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.SelectCommand.Parameters.AddWithValue("@Id", usu.Id);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public Mascota ValidarMascota(Mascota usu)
        {
            DataSet ds = new DataSet();
            
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "SELECT * FROM Mascotas WHERE Apodo = @Apodo ";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.SelectCommand.Parameters.AddWithValue("@Apodo", usu.Apodo);

                sda.Fill(ds);

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

                Mascota mas = (from DataRow dr in ds.Tables[0].Rows
                        select new Mascota()
                        {
                            Id = Convert.ToInt32(dr["IdMascota"]),
                            Apodo = dr["Apodo"]?.ToString() ?? "",
                            Nombre = dr["NombreMascota"]?.ToString() ?? "",
                            FechaNacimiento = Convert.ToDateTime(dr["FechaNacimiento"] ?? DateTime.Now),
                            Sexo = dr["Sexo"]?.ToString() ?? "",
                            Raza = dr["Raza"]?.ToString() ?? "",
                            Foto = (byte[])dr["Foto"],
                            IdCliente = Convert.ToInt32(dr["IdCliente"])
                        }).FirstOrDefault();

                return mas;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public Mascota UpdateMascota(Mascota usu)
        {
            try
            {

                SqlConnection con =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string query = "UPDATE Mascotas SET  "
                               + "[Apodo] = @Apodo "
                               + ",[NombreMascota] = @NombreMascota "
                               + ",[FechaNacimiento] = @FechaNacimiento "
                               + ",[Sexo] = @Sexo "
                               + ",[Raza] = @Raza "
                               + ",[IdCliente] = @IdCliente ";

                if (usu.Foto != null)
                    query += ",[Foto] = @Foto ";

                query += " WHERE IdMascota = @IdMascota ";

                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@IdMascota", usu.Id);
                cmd.Parameters.AddWithValue("@Apodo", usu.Apodo);
                cmd.Parameters.AddWithValue("@NombreMascota", usu.Nombre);
                cmd.Parameters.AddWithValue("@FechaNacimiento", usu.FechaNacimiento);

                if (usu.Foto != null)
                    cmd.Parameters.AddWithValue("@Foto", usu.Foto);

                cmd.Parameters.AddWithValue("@Sexo", usu.Sexo);
                cmd.Parameters.AddWithValue("@Raza", usu.Raza);
                cmd.Parameters.AddWithValue("@IdCliente", usu.IdCliente);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                Mascota mas = ValidarMascota(usu);
                return mas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Tareas

        public bool AddTareas(Tarea usu)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = @"INSERT INTO TareasMascotas (IdMascota, IdTarea, Fecha, Hora) Values( @IdMascota, @IdTarea, @Fecha, @Hora)";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@IdMascota", usu.IdMascota);
                cmd.Parameters.AddWithValue("@IdTarea", usu.IdTarea);
                cmd.Parameters.AddWithValue("@Fecha", usu.Fecha);
                cmd.Parameters.AddWithValue("@Hora", usu.Hora);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataSet GetTareas(string idCliente)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                string Query = "select *, CONVERT(VARCHAR(10), Fecha, 103) FechaTarea, CONVERT(VARCHAR(5), hora,108) HoraTarea from TareasMascotas tm " +
                               "inner join Tareas t on t.IdTarea = tm.IdTarea " +
                               "inner join TipoTareas tt on tt.IdTipo = t.IdTipo " +
                               "inner join Mascotas m on m.IdMascota = tm.IdMascota " +
                               "inner JOIN Usuarios u on u.Id = m.IdCliente " +
                               "where 1 = 1 ";

                if (!string.IsNullOrEmpty(idCliente)) Query += " And u.Id = " + idCliente;

                Query += " Order by NombreMascota, Fecha, Hora";

                SqlDataAdapter sda = new SqlDataAdapter(Query, con);
                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool DeleteTareas(Tarea usu)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string Query = "DELETE FROM TareasMascotas Where IdTareaMascota = @Id";

                cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@Id", usu.Id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        
        public bool UpdateTareas(Tarea usu)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);
                SqlCommand cmd = new SqlCommand();

                string query = "UPDATE TareasMascotas SET  "
                               + "[IdMascota] = @IdMascota "
                               + ",[IdTarea] = @IdTarea "
                               + ",[Fecha] = @Fecha "
                               + ",[Hora] = @Hora ";

                query += " WHERE IdTareaMascota = @IdTareaMascota ";

                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@IdTareaMascota", usu.Id);
                cmd.Parameters.AddWithValue("@IdMascota", usu.IdMascota);
                cmd.Parameters.AddWithValue("@IdTarea", usu.IdTarea);
                cmd.Parameters.AddWithValue("@Fecha", usu.Fecha);
                cmd.Parameters.AddWithValue("@Hora", usu.Hora);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

    }
}
